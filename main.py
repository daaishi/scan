# -*- coding: utf-8 -*-

import datetime
import locale
import cv2
import numpy as np
from PIL import Image

d = datetime.datetime.today()

imagePath = "source2.jpg"
resultPath = "result/" + d.strftime("%Y-%m-%d-%H-%M-%S") + ".png"

# 画像ファイル読み込み
sourceImage = cv2.imread(imagePath)
imageSize = sourceImage.shape
resizeWidth = imageSize[1]/2
resizeHeigt = imageSize[0]/2
sourceImage = cv2.resize(sourceImage, (resizeWidth, resizeHeigt))

# グレースケール変換
grayImage = cv2.cvtColor(sourceImage, cv2.COLOR_BGR2GRAY)
# cv2.imwrite('test-grayImage.png', grayImage)

# 2値化
# ret, thresh = cv2.threshold(grayImage, 127, 255, 0)
# thresh = cv2.adaptiveThreshold(grayImage, 127, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 7, 0)
thresh = cv2.adaptiveThreshold(grayImage, 127, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 7, 0)
# cv2.imwrite('test-thresh.png', thresh)

# 輪郭検出
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# cnt = contours[0]

# 短形を描く
# x,y,w,h = cv2.boundingRect(cnt)
# cv2.rectangle(sourceImage, (x,y), (x+w,y+h), (0,255,0), 1)
# cv2.drawContours(sourceImage, contours, -1, (255, 0, 0), 3)

maxArea = -1
maxCnt = None
for cnt in contours:
    area = cv2.contourArea(cnt)
    if area > maxArea:
        maxArea = area
        maxCnt = cnt

x,y,w,h = cv2.boundingRect(maxCnt)
offsetX = offsetY = 20
offsetW = offsetH = 40
rx = x + offsetX # 短形のx座標
ry = y + offsetY # 短形のy座業
rw = w - offsetW # 短形の幅
rh = h - offsetH #短形の高さ
cv2.rectangle(sourceImage, (rx, ry), (rx+rw, ry+rh), (0,255,0), 1)

# オリジナル画像の切り抜き
# trimImage = sourceImage[y:y+h, x:x+w]
# cv2.imwrite('trim.png', trimImage) # 画像の保存

# 白い部分を透明化
org = Image.open(imagePath)
transImage = Image.new('RGBA', org.size, (0, 0, 0, 0))
transImageW = org.size[0]
transImageH = org.size[1]
for x in xrange(transImageW):
    for y in xrange(transImageH):
        pixel = org.getpixel((x, y))
        if pixel[0] == 255 and pixel[1] == 255 and pixel[2] == 255:
            continue
        transImage.putpixel((x,y), pixel)

transImage = transImage.resize((transImageW/2, transImageH/2))
transImage = transImage.crop((rx, ry, rx+rw, ry+rh))
transImage.save(resultPath)

# ウインドウの表示
# cv2.imshow('Threshold Image', thresh)
# cv2.imshow("Edge Image", sourceImage)
# cv2.imshow("Trim Image", trimImage)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
